import { Component, OnInit, Inject } from '@angular/core';
import { ICharacter } from 'src/app/services/character.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface AddCharacterDialogData{
  character: ICharacter;
}

@Component({
  selector: 'app-add-character-dialog',
  templateUrl: './add-character-dialog.component.html',
  styleUrls: ['./add-character-dialog.component.scss']
})
export class AddCharacterDialogComponent implements OnInit {
  character: ICharacter;

  constructor(public dialogRef: MatDialogRef<AddCharacterDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: AddCharacterDialogData) { }

  ngOnInit() {
    this.character = this.data.character;
  }

  cancel(){
    this.dialogRef.close();
  }
}
