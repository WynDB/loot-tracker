"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importStar(require("mongoose"));
var crypto_1 = __importDefault(require("crypto"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var User = new mongoose_1.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    games: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: "Game"
        }],
    characters: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: "Character"
        }],
    invites: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: "Game"
        }],
    hash: {
        type: String
    },
    salt: {
        type: String
    }
});
User.methods.setPassword = function (password) {
    this.salt = crypto_1.default.randomBytes(16).toString("hex");
    this.hash = crypto_1.default.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString("hex");
};
User.methods.validPassword = function (password) {
    var hash = crypto_1.default.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};
User.methods.generateJwt = function () {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    return jsonwebtoken_1.default.sign({
        _id: this._id,
        email: this.email,
        firstName: this.firstName,
        lastName: this.lastName,
        exp: parseInt((expiry.getTime() / 1000).toString())
    }, "MY_SECRET");
};
exports.default = mongoose_1.default.model("User", User);
//# sourceMappingURL=user.model.js.map