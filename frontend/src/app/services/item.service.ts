import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { HttpClient } from '@angular/common/http';
import { GameService } from './game.service';
import * as env from '../../environments/environment';

export interface IItem {
  _id?: string,
  name: string;
  description: string;
  type: string;
  magical: boolean;
  claimedBy: any;
  game: any;
  quantity: number;
  availability: string;
  requiresAttunement: boolean;
  attunementRestrictions: string;
}

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  $items: BehaviorSubject<IItem[]> = new BehaviorSubject([]);
  $selectedItem: BehaviorSubject<IItem> = new BehaviorSubject(undefined);

  constructor(private http: HttpClient, private auth: AuthenticationService, private gameService: GameService) { }

  createItem(item: IItem) {
    const token = this.auth.getToken();
    this.http.post(`${env.environment.apiUrl}/api/items`, item, { headers: { Authorization: `Bearer ${token}` } }).subscribe((result: IItem) => {
      this.$items.next(this.$items.getValue().concat([result]));
    }, (err) => {
      console.log(err);
    });
  }

  getItem(id) {
    const token = this.auth.getToken();
    this.http.get(`${env.environment.apiUrl}/api/items/${id}`, { headers: { Authorization: `Bearer ${token}` } }).subscribe((item: IItem) => {
      this.$selectedItem.next(item);
    }, (err) => {
      console.log(err);
    });
  }

  listItems(gameId) {
    const token = this.auth.getToken();
    this.http.get(`${env.environment.apiUrl}/api/items/list/${gameId}`, { headers: { Authorization: `Bearer ${token}` } }).subscribe((items: IItem[]) => {
      this.$items.next(items);
    }, (err) => {
      console.log(err);
    });
  }

  claimItem(itemId, characterId) {
    const game = this.gameService.$selectedGame.getValue();
    const token = this.auth.getToken();
    this.http.put(`${env.environment.apiUrl}/api/items/claim/${itemId}`, { game: game._id, character: characterId }, { headers: { Authorization: `Bearer ${token}` } }).subscribe(() => {
      this.listItems(game._id);
    });
  }

  releaseItem(itemId, characterId) {
    const game = this.gameService.$selectedGame.getValue();
    const token = this.auth.getToken();
    this.http.put(`${env.environment.apiUrl}/api/items/release/${itemId}`, { game: game._id, character: characterId }, { headers: { Authorization: `Bearer ${token}` } }).subscribe(() => {
      this.listItems(game._id);
    });
  }

  updateItem(item: IItem){
    const game = this.gameService.$selectedGame.getValue();
    const token = this.auth.getToken();
    this.http.put(`${env.environment.apiUrl}/api/items`, item, { headers: { Authorization: `Bearer ${token}` } }).subscribe((itemResult)=>{
      this.listItems(game._id);
    });
  }
}
