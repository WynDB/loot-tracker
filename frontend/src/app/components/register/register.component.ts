import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  host: {
    class: "component-host"
  }
})
export class RegisterComponent implements OnInit {
  firstName: string;
  lastName: string;
  email: string;
  organisation: string;
  password: string;

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  register(){
    this.auth.register({email: this.email, firstName: this.firstName, lastName: this.lastName, password: this.password}).subscribe(()=>{
      this.router.navigateByUrl('/profile');
    }, (err)=>{
      console.log(err);
    });
  }
}
