import Game, { IGameModel } from "../models/game.model";
import User, {IUserModel} from "../models/user.model";
import Character from "../models/character.model";
import Item from "../models/item.model";
import { Response, NextFunction } from "express";

export const create = async (req: any, res: Response, next: NextFunction)=>{
    try{
        if(!req.payload){
            return res.status(401).send(`Unauthorised: Please log in.`);            
        }

        const user = await User.findById(req.payload._id) as IUserModel;
        if(!user){
            return res.status(404).send(`Invalid User.`);
        }

        const game = await Game.create({
            title: req.body.title,
            system: req.body.system,
            description: req.body.description,
            users: [user._id]
        });

        if(!user.games){
            user.games = [];
        }

        user.games.push(game._id);
        await user.save();

        res.status(200).json(game);
    }catch(err){
        console.log(err);
        next(err);
    }
}

export const list = async (req: any, res: Response, next: NextFunction)=>{
    console.log(`Listing Games`);
    try{
        if(!req.payload){
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id).populate("games").exec() as IUserModel;
        if(!user){
            return res.status(404).send(`Invalid User.`);
        }

        res.status(200).json(user.games);
    }catch(err){
        next(err);
    }
}

export const read = async (req: any, res: Response, next: NextFunction)=>{
    try{
        if(!req.payload){
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id) as IUserModel;
        if(!user){
            return res.status(404).send(`Invalid User.`);
        }

        const game = await Game.findById(req.params.id)
            .populate({path: "characters", model: Character, select: "name"})
            .populate({path: "items", model: Item, select: "name"})
            .populate({path: "invitees", model: User, select: "email"}).exec();

        if(!game){
            return res.status(404).send(`Game could not be found.`);
        }

        if(user.games.indexOf(game._id.toString())>=0){
            return res.status(200).json(game);
        }else{
            return res.status(404).send(`Unauthorised: You do not have access to this game.`);
        }
    }catch(err){
        next(err);
    }
}

export const update = async (req: any, res: Response, next: NextFunction)=>{
    try{
        if(!req.payload){
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id).populate("games").exec() as IUserModel;
        if(!user){
            return res.status(404).send(`Invalid User.`);
        }

        const game = await Game.findById(req.params.id) as IGameModel;
        if(!game){
            return res.status(404).send(`Game could not be found.`);
        }

        if(user.games.indexOf(game._id)>=0){
            game.title = req.body.title;
            game.description = req.body.description;
            game.system = req.body.system;
            await game.save();
            return res.status(200).send(true);
        }else{
            return res.status(404).send(`Unauthorised: You do not have access to this game.`);
        }
    }catch(err){
        next(err);
    }
}

export const remove = async (req: any, res: Response, next: NextFunction)=>{
    try{
        if(!req.payload){
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id).populate("games").exec() as IUserModel;
        if(!user){
            return res.status(404).send(`Invalid User.`);
        }

        const game = await Game.findById(req.params.id) as IGameModel;
        if(!game){
            return res.status(404).send(`Game could not be found.`);
        }

        if(user.games.indexOf(game._id)>=0){
            await Game.findOneAndRemove({_id: game._id});
            return res.status(200).send(true);
        }else{
            return res.status(404).send(`Unauthorised: You do not have access to this game.`);
        }
    }catch(err){
        next(err);
    }
}