import express from "express";
const routes = express.Router();

import * as controller from "../controllers/game.controller";

routes.post("", controller.create);
routes.get("", controller.list);
routes.get("/:id", controller.read);
routes.put("", controller.update);
routes.delete("/:id", controller.remove);

export default routes;