import { Component, OnInit } from '@angular/core';
import { GameService, IGame } from 'src/app/services/game.service';
import { MatDialog } from '@angular/material';
import { AddGameDialogComponent } from '../dialogs/add-game-dialog/add-game-dialog.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  games: IGame[];
  selectedGame: IGame;

  constructor(private gameService: GameService, public matDialog: MatDialog) { }

  ngOnInit() {
    this.gameService.$allGames.subscribe((games: IGame[])=>{
      this.games = games;
    });
    this.gameService.$selectedGame.subscribe((game: IGame)=>{
      this.selectedGame = game;
    });

    this.fetchGames();
  }

  fetchGames() {
    this.gameService.listGames();
  }

  openAddGameDialog(){
    const dialogRef = this.matDialog.open(AddGameDialogComponent, {data: {game: {title: "My New Game"}}})
    dialogRef.afterClosed().subscribe((game: IGame)=>{
      if(game){
        this.createGame(game);
      }
    });
  };

  createGame(game: IGame){
    this.gameService.createGame(game, ()=>{
      this.fetchGames();
    });
  }

  selectGame(id){
    this.gameService.getGame(id);
  }
}
