import { join } from "path";
import { format, transports, Logger, createLogger } from "winston";

const loggers: { [index: string]: Logger } = {};

export const initialiseLogger = (name: string) => {
    const logger = createLogger({
        level: "info",
        format: format.combine(format.timestamp(), format.json(), format.prettyPrint()),
        transports: [
            new transports.File({ 
                filename: join(process.cwd(), 'logs/backend/error.log'), 
                level: 'error' 
            }),
            new transports.File({ 
                filename: join(process.cwd(), 'logs/backend/info.log'), 
                level: 'info' 
            }),
            new transports.File({ 
                filename: join(process.cwd(), 'logs/backend/combined.log') 
            }),
            new transports.Console({
                level: 'debug',
                format: format.combine(format.colorize(),
                    format.align(),
                    format.timestamp(),
                    format.printf(info => `${info.timestamp} [${info.level}]: ${info.message.trim()}`))
            })
        ],
        exceptionHandlers: [
            new transports.File({ filename: join(process.cwd(), 'logs/backend/exceptions.log') })
        ]
    });

    logger.exitOnError = false;

    loggers[name] = logger;
}

export const getLogger = (name: string): Logger => {
    return loggers[name];
}