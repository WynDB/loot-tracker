import passport from "passport";
import mongoose from "mongoose";

import User, { IUserModel } from "../models/user.model";

const LocalStrategy = require("passport-local").Strategy;

passport.use(new LocalStrategy({
    usernameField: "email"
},
function(username: string, password: string, done: any){
    User.findOne({email: username}, function(err, user: IUserModel){
        if(err){
            return done(err);
        }

        if(!user){
            return done(null, false, {
                message: "User not found"
            });
        }

        if(!user.validPassword(password)){
            return done(null, false, {
                message: "Incorrect password"
            });
        }

        return done(user);
    });
}));