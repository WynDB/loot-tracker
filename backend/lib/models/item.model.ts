import mongoose, { Document, Schema } from "mongoose";

export interface IItem {
    name: string;
    description: string;
    type: string;
    magical: boolean;
    claimedBy: any;
    game: any;
    quantity: number;
    availability: string;
    requiresAttunement: boolean;
    attunementRestrictions: string;
}

export interface IItemModel extends IItem, Document {
    _id: string;
}

const Item: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    type: {
        type: String,
    },
    quantity: {
        type: Number
    },
    magical: {
        type: Boolean
    },
    availability: {
        type: String
    },
    requiresAttunement: {
        type: Boolean
    },
    attunementRestrictions: {
        type: String
    },
    game: {
        type: Schema.Types.ObjectId,
        ref: "Game"
    },
    claimedBy: {
        type: Schema.Types.ObjectId,
        ref: "Character"
    }
}, {
    timestamps: true
});

export default mongoose.model("Item", Item);