import express, { Request, Response } from "express";
import cors from "cors";
import bodyParser from "body-parser";
import passport from "passport";
import mongoose from "mongoose";
import { initialiseLogger, getLogger } from "./winston";

require("dotenv").config();
require("./logic/passport");

import routes from "./routes/index.routes";

initialiseLogger("main");

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.use("/api", routes);

app.use((req, res)=>{
    res.send(`Express server is running on port ${process.env.PORT || 3000}`);
});

app.use(function (err: Error, req: Request, res: Response) {
    const logger = getLogger("main");
    logger.error({message: err.message, error: err.stack, name: err.name });
    if (err.name === "UnauthorizedError") {
        return res.status(401).json({ message: `${err.name}: ${err.message}` });
    }
    return res.status(500).json({ message: `Internal Server Error` });
});

const mongoUrl = process.env.MONGODB_URL as string;

mongoose.connect(mongoUrl, { useNewUrlParser: true });
mongoose.Promise = Promise;
const connection = mongoose.connection;

connection.once('open', () => {
    const logger = getLogger("main");
    logger.info(`MongoDB database connection established successfully.`);
});

app.listen(process.env.PORT || 3000, () => {
    const logger = getLogger("main");
    logger.info(`Express server running on port 3000`)
});