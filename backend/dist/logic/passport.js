"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var passport_1 = __importDefault(require("passport"));
var user_model_1 = __importDefault(require("../models/user.model"));
var LocalStrategy = require("passport-local").Strategy;
passport_1.default.use(new LocalStrategy({
    usernameField: "email"
}, function (username, password, done) {
    user_model_1.default.findOne({ email: username }, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, {
                message: "User not found"
            });
        }
        if (!user.validPassword(password)) {
            return done(null, false, {
                message: "Incorrect password"
            });
        }
        return done(user);
    });
}));
//# sourceMappingURL=passport.js.map