import Item, { IItemModel } from "../models/item.model";
import User, { IUserModel } from "../models/user.model";
import Game, { IGameModel } from "../models/game.model";
import Character, { ICharacterModel } from "../models/character.model";
import { Response, NextFunction } from "express";

export const create = async (req: any, res: Response, next: NextFunction) => {
    try {
        console.log(`---Creating Item with Body:`);
        console.log(req.body);

        if (!req.payload) {
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id) as IUserModel;
        if (!user) {
            return res.status(404).send(`Invalid User.`);
        }

        //First get the game the character is supposed to be a part of. If the user does not have access to that game, the process should not complete.
        if (!req.body.game) {
            return res.status(404).send(`Invalid Game.`);
        }

        const game = await Game.findById(req.body.game) as IGameModel;
        if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
            return res.status(404).send(`You are not a part of this Game.`);
        }

        const item = await Item.create({
            name: req.body.name,
            description: req.body.description,
            type: req.body.type,
            magical: req.body.magical,
            game: game._id,
            quantity: req.body.quantity,
            availability: req.body.availability,
            requiresAttunement: req.body.requiresAttunement,
            attunementRestrictions: req.body.attunementRestrictions         
        }) as IItemModel;

        game.items.push(item._id);
        await game.save();

        if(req.body.claimedBy){
            const character = await Character.findById(req.body.claimedBy) as ICharacterModel;
            if(character){
                item.claimedBy = character._id;
                await item.save();

                character.items.push(item._id);
                await character.save();
            }            
        }

        return res.json(item);
    } catch (err) {
        next(err);
    }
}

export const read = async (req: any, res: Response, next: NextFunction) => {
    try {
        res.send(true);
    } catch (err) {
        next(err);
    }
}

export const update = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (!req.payload) {
            return res.status(401).send(`Unauthorised: Please log in.`);
        }
    
        const user = await User.findById(req.payload._id) as IUserModel;
        if (!user) {
            return res.status(404).send(`Invalid User.`);
        }
    
        const game = await Game.findById(req.body.game) as IGameModel;
        if(!game){
            return res.status(404).send(`Invalid Game`);
        }
    
        const item = await Item.findById(req.body._id) as IItemModel;
        if(!item){
            return res.status(404).send(`Invalid Item.`);
        }
    
        if(item.claimedBy){
            const character = await Character.findById(item.claimedBy) as ICharacterModel;
            if(character && user.characters.find(c=>c._id===character._id)){
                return res.status(404).send(`You do not control the character that has claimed this item.`);
            }
        }
    
        if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
            return res.status(404).send(`You are not a part of this Game.`);
        }
    
        item.name = req.body.name || item.name;
        item.description = req.body.description;
        item.type = req.body.type;
        item.magical = req.body.magical;
        item.game = game._id;
        item.quantity = req.body.quantity;
        item.availability = req.body.availability;
        item.requiresAttunement = req.body.requiresAttunement;
        item.attunementRestrictions = req.body.attunementRestrictions;

        await item.save();
        return res.status(200).json(item);
    } catch (err) {
        next(err);
    }
}

export const remove = async (req: any, res: Response, next: NextFunction) => {
    try {
        res.send(true);
    } catch (err) {
        next(err);
    }
}

export const list = async (req: any, res: Response, next: NextFunction) => {
    if (!req.payload) {
        return res.status(401).send(`Unauthorised: Please log in.`);
    }

    const user = await User.findById(req.payload._id) as IUserModel;
    if (!user) {
        return res.status(404).send(`Invalid User.`);
    }

    const game = await Game.findById(req.params.id).populate({ path: "items", model: Item }) as IGameModel;
    if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
        return res.status(404).send(`You are not a part of this Game.`);
    }

    return res.json(game.items);
}

export const claim = async (req: any, res: Response, next: NextFunction) => {
    if (!req.payload) {
        return res.status(401).send(`Unauthorised: Please log in.`);
    }

    const user = await User.findById(req.payload._id) as IUserModel;
    if (!user) {
        return res.status(404).send(`Invalid User.`);
    }

    const game = await Game.findById(req.body.game).populate({ path: "characters", model: Character }) as IGameModel;
    if(!game){
        return res.status(404).send(`Invalid Game`);
    }

    const item = await Item.findById(req.params.id) as IItemModel;
    if(!item){
        return res.status(404).send(`Invalid Item.`);
    }

    const character = await Character.findById(req.body.character) as ICharacterModel;
    if(!character){
        return res.status(404).send(`Invalid Character.`);
    }

    if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
        return res.status(404).send(`You are not a part of this Game.`);
    }

    if(item.claimedBy){
        return res.status(404).send(`Item already claimed!`);
    }

    item.claimedBy = character._id;
    await item.save();

    character.items.push(item._id);
    await character.save();

    return res.status(200).json({item: item, character: character});
}

export const release = async (req: any, res: Response, next: NextFunction) => {
    if (!req.payload) {
        return res.status(401).send(`Unauthorised: Please log in.`);
    }

    const user = await User.findById(req.payload._id) as IUserModel;
    if (!user) {
        return res.status(404).send(`Invalid User.`);
    }

    const game = await Game.findById(req.body.game).populate({ path: "characters", model: Character }) as IGameModel;
    if(!game){
        return res.status(404).send(`Invalid Game`);
    }

    const item = await Item.findById(req.params.id) as IItemModel;
    if(!item){
        return res.status(404).send(`Invalid Item.`);
    }

    const character = await Character.findById(req.body.character) as ICharacterModel;
    if(!character){
        return res.status(404).send(`Invalid Character.`);
    }

    if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
        return res.status(404).send(`You are not a part of this Game.`);
    }

    if(!item.claimedBy){
        return res.status(404).send(`Item already unclaimed!`);
    }

    item.claimedBy = undefined;
    await item.save();

    await Character.update({}, {$pull: {items: item._id}});

    return res.status(200).json({item: item, character: character});
}