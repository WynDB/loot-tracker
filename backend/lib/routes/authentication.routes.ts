import express from "express";
import jwt from "express-jwt";
const router = express.Router();

import * as controller from "../controllers/authentication.controller";

const auth = jwt({
    secret: 'MY_SECRET',
    userProperty: "payload"
});

router.post("/register", controller.register);
router.post("/login", controller.login);
router.get("/profile/:id", auth, controller.getUser);

export default router;