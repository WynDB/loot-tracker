import { Component, OnInit } from '@angular/core';
import { GameService, IGame } from 'src/app/services/game.service';
import { MatDialog } from '@angular/material';
import { AddGameDialogComponent } from '../dialogs/add-game-dialog/add-game-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-panel',
  templateUrl: './game-panel.component.html',
  styleUrls: ['./game-panel.component.scss'],
  host: {
    class: "component-host"
  }
})
export class GamePanelComponent implements OnInit {
  games: IGame[];
  selectedGame: IGame;

  constructor(public matDialog: MatDialog, private router: Router, private gameService: GameService) { }

  ngOnInit() {
    this.gameService.$allGames.subscribe((games: IGame[])=>{
      this.games = games;
    });

    this.gameService.$selectedGame.subscribe((game: IGame)=>{
      this.selectedGame = game;
    });

    this.gameService.listGames();
  }

  openAddGameDialog(){
    const dialogRef = this.matDialog.open(AddGameDialogComponent, {data: {game: {title: "My New Game"}}})
    dialogRef.afterClosed().subscribe((game: IGame)=>{
      if(game){
        this.createGame(game);
      }
    });
  }

  createGame(game: IGame){
    this.gameService.createGame(game, ()=>{
      this.gameService.listGames();
    });
  }

  selectGame(id){
    this.router.navigateByUrl(`/game/${id}`);
  }

  goToUserList(id){
    this.router.navigateByUrl(`/game/${id}/users`);
  }
}
