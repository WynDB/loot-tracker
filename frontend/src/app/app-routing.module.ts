import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { GameDetailsComponent } from './components/game-details/game-details.component';
import { GameUserListComponent } from './components/game-user-list/game-user-list.component';


const routes: Routes = [{
  path: '', component: HomeComponent
},{
  path: 'game/:id', component: GameDetailsComponent
},{
  path: 'game/:id/users', component: GameUserListComponent
},{
  path: 'login', component: LoginComponent
},{
  path: 'register', component: RegisterComponent
},{
  path: 'profile', component: ProfileComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
