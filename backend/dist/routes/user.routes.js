"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var router = express_1.default.Router();
var controller = __importStar(require("../controllers/user.controller"));
router.post("", controller.create);
router.get("/:id", controller.read);
router.put("/:id", controller.update);
router.put("/invite", controller.invite);
router.put("/acceptinvite/:id", controller.acceptInvite);
router.delete("/:id", controller.remove);
exports.default = router;
//# sourceMappingURL=user.routes.js.map