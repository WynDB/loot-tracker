import { Injectable } from '@angular/core';
import * as env from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { BehaviorSubject } from 'rxjs';

export interface IUser {
  _id?: string;
  firstName: string;
  lastName: string;
  email: string;
  characters: any[];
  games: any[];
  invites: any[];
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  $selectedUser: BehaviorSubject<IUser> = new BehaviorSubject(undefined);

  selectUser(id: string) {
    const token = this.auth.getToken();
    this.http.get(`${env.environment.apiUrl}/api/users/${id}`, { headers: { Authorization: `Bearer ${token}` } }).subscribe((user: IUser) => {
      this.$selectedUser.next(user);
    })
  }

  inviteToGame(userId, gameId) {
    const token = this.auth.getToken();
    this.http.put(`${env.environment.apiUrl}/api/users/invite`, { email: userId, game: gameId }, { headers: { Authorization: `Bearer ${token}` } }).subscribe(() => {
    });
  }

  acceptGameInvite(userId, gameId) {
    const token = this.auth.getToken();
    this.http.put(`${env.environment.apiUrl}/api/users/acceptinvite/${userId}`, { game: gameId }, { headers: { Authorization: `Bearer ${token}` } }).subscribe(() => {
    });
  }
}
