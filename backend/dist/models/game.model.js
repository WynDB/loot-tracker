"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importStar(require("mongoose"));
var Game = new mongoose_1.Schema({
    title: {
        type: String
    },
    system: {
        type: String
    },
    description: {
        type: String
    },
    items: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "Item"
        }],
    characters: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "Character"
        }],
    users: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "User"
        }],
    invitees: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "User"
        }]
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model("Game", Game);
//# sourceMappingURL=game.model.js.map