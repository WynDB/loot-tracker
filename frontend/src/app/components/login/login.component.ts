import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  host: {
    class: "component-host"
  }
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(private router: Router, private auth: AuthenticationService) { }

  ngOnInit() {
  }

  login(){
    this.auth.login({email: this.email, password: this.password}).subscribe(()=>{
      this.router.navigateByUrl('');
    });
  }
}
