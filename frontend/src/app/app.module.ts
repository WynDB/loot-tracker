import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { 
  MatToolbarModule, 
  MatSidenavModule, 
  MatListModule, 
  MatButtonModule, 
  MatCardModule, 
  MatFormFieldModule, 
  MatSelectModule, 
  MatMenuModule, 
  MatDialogModule, 
  MatInputModule, 
  MatTableModule, 
  MatIconModule,
  MatLineModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatCheckboxModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AddGameDialogComponent } from './components/dialogs/add-game-dialog/add-game-dialog.component';
import { GamePanelComponent } from './components/game-panel/game-panel.component';
import { GameDetailsComponent } from './components/game-details/game-details.component';
import { AddCharacterDialogComponent } from './components/dialogs/add-character-dialog/add-character-dialog.component';
import { AddItemDialogComponent } from './components/dialogs/add-item-dialog/add-item-dialog.component';
import { EditGameDialogComponent } from './components/dialogs/edit-game-dialog/edit-game-dialog.component';
import { EditItemDialogComponent } from './components/dialogs/edit-item-dialog/edit-item-dialog.component';
import { EditCharacterDialogComponent } from './components/dialogs/edit-character-dialog/edit-character-dialog.component';
import { GameUserListComponent } from './components/game-user-list/game-user-list.component';
import { ItemCardComponent } from './components/cards/item-card/item-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    ToolbarComponent,
    AddGameDialogComponent,
    GamePanelComponent,
    GameDetailsComponent,
    AddCharacterDialogComponent,
    AddItemDialogComponent,
    EditGameDialogComponent,
    EditItemDialogComponent,
    EditCharacterDialogComponent,
    GameUserListComponent,
    ItemCardComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatLineModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatTableModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AddCharacterDialogComponent,
    AddGameDialogComponent,
    AddItemDialogComponent,
    EditGameDialogComponent,
    EditItemDialogComponent
  ]
})
export class AppModule { }
