import { Component, OnInit } from '@angular/core';
import { GameService, IGame } from 'src/app/services/game.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AddCharacterDialogComponent } from '../dialogs/add-character-dialog/add-character-dialog.component';
import { ICharacter, CharacterService } from 'src/app/services/character.service';
import { AddItemDialogComponent } from '../dialogs/add-item-dialog/add-item-dialog.component';
import { IItem, ItemService } from 'src/app/services/item.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss'],
  host: {
    class: "component-host"
  }
})
export class GameDetailsComponent implements OnInit {
  game: IGame;
  characters: ICharacter[] = [];
  userCharacters: ICharacter[] = [];
  claimant: ICharacter;
  items: IItem[] = [];
  claimedItems: {[index: string]: IItem[]} = {};
  unclaimedItems: IItem[] = [];

  constructor(
    public matDialog: MatDialog, 
    private activatedRoute: ActivatedRoute, 
    private gameService: GameService, 
    private characterService: CharacterService,
    private itemService: ItemService,
    private auth: AuthenticationService
  ) { }

  ngOnInit() {
    this.gameService.$selectedGame.subscribe((game: IGame) => {
      this.game = game;

      if(this.game){
        this.characterService.listCharacters(this.game._id);
        this.itemService.listItems(this.game._id);
      }
    });

    this.characterService.$allCharacters.subscribe((characters: ICharacter[])=>{
      this.characters = characters;
      this.userCharacters = characters.filter((c)=>{return c.user===this.auth.getUserDetails()._id});
    });

    this.itemService.$items.subscribe((items: IItem[])=>{
      this.claimedItems = {};
      this.items = items;
      this.unclaimedItems = items.filter((i)=>{return !i.claimedBy});
      items.filter((i)=>{return i.claimedBy!=undefined}).forEach((i)=>{
        if(!this.claimedItems[i.claimedBy]){
          this.claimedItems[i.claimedBy] = [];
        }
        this.claimedItems[i.claimedBy].push(i);
      });
    });

    const id = this.activatedRoute.snapshot.params["id"];
    this.gameService.getGame(id);
  }

  openAddCharacterDialog() {
    const dialogRef = this.matDialog.open(AddCharacterDialogComponent, { data: { character: { name: "My New Character", game: this.game._id } } });
    dialogRef.afterClosed().subscribe((character: ICharacter) => {
      if (character) {
        this.createCharacter(character);
      }
    });
  }

  openAddItemDialog() {
    const dialogRef = this.matDialog.open(AddItemDialogComponent, { 
      data: { 
        item: { 
          name: "My New Item", 
          game: this.game._id, 
          quantity: 1, 
          availability: "common", 
          magical: false, 
          requiresAttunement: false
        } as IItem
      }
    });

    dialogRef.afterClosed().subscribe((item: IItem) => {
      if (item) {
        this.createItem(item);
      }
    });
  }

  onSelectClaimant(character: ICharacter){
    this.claimant = character;
  }

  createCharacter(character: ICharacter) {
    this.characterService.createCharacter(character);
  }

  createItem(item: IItem){
    this.itemService.createItem(item);
  }

  claimItem(item: IItem){
    this.itemService.claimItem(item._id, this.claimant._id)
  }

  releaseItem(item: IItem, ownerId: string){
    this.itemService.releaseItem(item._id, ownerId);
  }

  getCharacterItems(characterId){
    return this.items.filter((i)=>{return i.claimedBy = characterId});
  }

  saveItem(item: IItem){
    console.log(item);
    this.itemService.updateItem(item);
  }
}
