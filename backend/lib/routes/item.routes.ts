import express from "express";
const routes = express.Router();

import * as controller from "../controllers/item.controller";

routes.post("", controller.create);
routes.get("/:id", controller.read);
routes.get("/list/:id", controller.list);
routes.put("", controller.update);
routes.put("/claim/:id", controller.claim);
routes.put("/release/:id", controller.release);
routes.delete("/:id", controller.remove);

export default routes;