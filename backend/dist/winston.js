"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var winston_1 = require("winston");
var loggers = {};
exports.initialiseLogger = function (name) {
    var logger = winston_1.createLogger({
        level: "info",
        format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.json(), winston_1.format.prettyPrint()),
        transports: [
            new winston_1.transports.File({
                filename: path_1.join(process.cwd(), 'logs/backend/error.log'),
                level: 'error'
            }),
            new winston_1.transports.File({
                filename: path_1.join(process.cwd(), 'logs/backend/info.log'),
                level: 'info'
            }),
            new winston_1.transports.File({
                filename: path_1.join(process.cwd(), 'logs/backend/combined.log')
            }),
            new winston_1.transports.Console({
                level: 'debug',
                format: winston_1.format.combine(winston_1.format.colorize(), winston_1.format.align(), winston_1.format.timestamp(), winston_1.format.printf(function (info) { return info.timestamp + " [" + info.level + "]: " + info.message.trim(); }))
            })
        ],
        exceptionHandlers: [
            new winston_1.transports.File({ filename: path_1.join(process.cwd(), 'logs/backend/exceptions.log') })
        ]
    });
    logger.exitOnError = false;
    loggers[name] = logger;
};
exports.getLogger = function (name) {
    return loggers[name];
};
//# sourceMappingURL=winston.js.map