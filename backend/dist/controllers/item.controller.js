"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var item_model_1 = __importDefault(require("../models/item.model"));
var user_model_1 = __importDefault(require("../models/user.model"));
var game_model_1 = __importDefault(require("../models/game.model"));
var character_model_1 = __importDefault(require("../models/character.model"));
exports.create = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, item, character, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 9, , 10]);
                console.log("---Creating Item with Body:");
                console.log(req.body);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                //First get the game the character is supposed to be a part of. If the user does not have access to that game, the process should not complete.
                if (!req.body.game) {
                    return [2 /*return*/, res.status(404).send("Invalid Game.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.body.game)];
            case 2:
                game = _a.sent();
                if (game.users.map(function (c) { return c.toString(); }).indexOf(req.payload._id) < 0) {
                    return [2 /*return*/, res.status(404).send("You are not a part of this Game.")];
                }
                return [4 /*yield*/, item_model_1.default.create({
                        name: req.body.name,
                        description: req.body.description,
                        type: req.body.type,
                        magical: req.body.magical,
                        game: game._id,
                        quantity: req.body.quantity,
                        availability: req.body.availability,
                        requiresAttunement: req.body.requiresAttunement,
                        attunementRestrictions: req.body.attunementRestrictions
                    })];
            case 3:
                item = _a.sent();
                game.items.push(item._id);
                return [4 /*yield*/, game.save()];
            case 4:
                _a.sent();
                if (!req.body.claimedBy) return [3 /*break*/, 8];
                return [4 /*yield*/, character_model_1.default.findById(req.body.claimedBy)];
            case 5:
                character = _a.sent();
                if (!character) return [3 /*break*/, 8];
                item.claimedBy = character._id;
                return [4 /*yield*/, item.save()];
            case 6:
                _a.sent();
                character.items.push(item._id);
                return [4 /*yield*/, character.save()];
            case 7:
                _a.sent();
                _a.label = 8;
            case 8: return [2 /*return*/, res.json(item)];
            case 9:
                err_1 = _a.sent();
                next(err_1);
                return [3 /*break*/, 10];
            case 10: return [2 /*return*/];
        }
    });
}); };
exports.read = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        try {
            res.send(true);
        }
        catch (err) {
            next(err);
        }
        return [2 /*return*/];
    });
}); };
exports.update = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, item, character_1, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 7, , 8]);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.body.game)];
            case 2:
                game = _a.sent();
                if (!game) {
                    return [2 /*return*/, res.status(404).send("Invalid Game")];
                }
                return [4 /*yield*/, item_model_1.default.findById(req.body._id)];
            case 3:
                item = _a.sent();
                if (!item) {
                    return [2 /*return*/, res.status(404).send("Invalid Item.")];
                }
                if (!item.claimedBy) return [3 /*break*/, 5];
                return [4 /*yield*/, character_model_1.default.findById(item.claimedBy)];
            case 4:
                character_1 = _a.sent();
                if (character_1 && user.characters.find(function (c) { return c._id === character_1._id; })) {
                    return [2 /*return*/, res.status(404).send("You do not control the character that has claimed this item.")];
                }
                _a.label = 5;
            case 5:
                if (game.users.map(function (c) { return c.toString(); }).indexOf(req.payload._id) < 0) {
                    return [2 /*return*/, res.status(404).send("You are not a part of this Game.")];
                }
                item.name = req.body.name || item.name;
                item.description = req.body.description;
                item.type = req.body.type;
                item.magical = req.body.magical;
                item.game = game._id;
                item.quantity = req.body.quantity;
                item.availability = req.body.availability;
                item.requiresAttunement = req.body.requiresAttunement;
                item.attunementRestrictions = req.body.attunementRestrictions;
                return [4 /*yield*/, item.save()];
            case 6:
                _a.sent();
                return [2 /*return*/, res.status(200).json(item)];
            case 7:
                err_2 = _a.sent();
                next(err_2);
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); };
exports.remove = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        try {
            res.send(true);
        }
        catch (err) {
            next(err);
        }
        return [2 /*return*/];
    });
}); };
exports.list = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.params.id).populate({ path: "items", model: item_model_1.default })];
            case 2:
                game = _a.sent();
                if (game.users.map(function (c) { return c.toString(); }).indexOf(req.payload._id) < 0) {
                    return [2 /*return*/, res.status(404).send("You are not a part of this Game.")];
                }
                return [2 /*return*/, res.json(game.items)];
        }
    });
}); };
exports.claim = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, item, character;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.body.game).populate({ path: "characters", model: character_model_1.default })];
            case 2:
                game = _a.sent();
                if (!game) {
                    return [2 /*return*/, res.status(404).send("Invalid Game")];
                }
                return [4 /*yield*/, item_model_1.default.findById(req.params.id)];
            case 3:
                item = _a.sent();
                if (!item) {
                    return [2 /*return*/, res.status(404).send("Invalid Item.")];
                }
                return [4 /*yield*/, character_model_1.default.findById(req.body.character)];
            case 4:
                character = _a.sent();
                if (!character) {
                    return [2 /*return*/, res.status(404).send("Invalid Character.")];
                }
                if (game.users.map(function (c) { return c.toString(); }).indexOf(req.payload._id) < 0) {
                    return [2 /*return*/, res.status(404).send("You are not a part of this Game.")];
                }
                if (item.claimedBy) {
                    return [2 /*return*/, res.status(404).send("Item already claimed!")];
                }
                item.claimedBy = character._id;
                return [4 /*yield*/, item.save()];
            case 5:
                _a.sent();
                character.items.push(item._id);
                return [4 /*yield*/, character.save()];
            case 6:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ item: item, character: character })];
        }
    });
}); };
exports.release = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, item, character;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.body.game).populate({ path: "characters", model: character_model_1.default })];
            case 2:
                game = _a.sent();
                if (!game) {
                    return [2 /*return*/, res.status(404).send("Invalid Game")];
                }
                return [4 /*yield*/, item_model_1.default.findById(req.params.id)];
            case 3:
                item = _a.sent();
                if (!item) {
                    return [2 /*return*/, res.status(404).send("Invalid Item.")];
                }
                return [4 /*yield*/, character_model_1.default.findById(req.body.character)];
            case 4:
                character = _a.sent();
                if (!character) {
                    return [2 /*return*/, res.status(404).send("Invalid Character.")];
                }
                if (game.users.map(function (c) { return c.toString(); }).indexOf(req.payload._id) < 0) {
                    return [2 /*return*/, res.status(404).send("You are not a part of this Game.")];
                }
                if (!item.claimedBy) {
                    return [2 /*return*/, res.status(404).send("Item already unclaimed!")];
                }
                item.claimedBy = undefined;
                return [4 /*yield*/, item.save()];
            case 5:
                _a.sent();
                return [4 /*yield*/, character_model_1.default.update({}, { $pull: { items: item._id } })];
            case 6:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ item: item, character: character })];
        }
    });
}); };
//# sourceMappingURL=item.controller.js.map