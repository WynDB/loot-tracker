import express from "express";
const routes = express.Router();

import * as controller from "../controllers/character.controller";

routes.post("", controller.create);
routes.get("/:id", controller.read);
routes.get("/list/:id", controller.list);
routes.put("", controller.update);
routes.delete("/:id", controller.remove);

export default routes;