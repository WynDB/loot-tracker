"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("cors"));
var body_parser_1 = __importDefault(require("body-parser"));
var passport_1 = __importDefault(require("passport"));
var mongoose_1 = __importDefault(require("mongoose"));
var winston_1 = require("./winston");
require("dotenv").config();
require("./logic/passport");
var index_routes_1 = __importDefault(require("./routes/index.routes"));
winston_1.initialiseLogger("main");
var app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
app.use(passport_1.default.initialize());
app.use("/api", index_routes_1.default);
app.use(function (err, req, res) {
    var logger = winston_1.getLogger("main");
    logger.error({ message: err.message, error: err.stack, name: err.name });
    if (err.name === "UnauthorizedError") {
        return res.status(401).json({ message: err.name + ": " + err.message });
    }
    return res.status(500).json({ message: "Internal Server Error" });
});
var mongoUrl = process.env.MONGODB_URL;
mongoose_1.default.connect(mongoUrl, { useNewUrlParser: true });
mongoose_1.default.Promise = Promise;
var connection = mongoose_1.default.connection;
connection.once('open', function () {
    var logger = winston_1.getLogger("main");
    logger.info("MongoDB database connection established successfully.");
});
app.listen(process.env.PORT || 3000, function () {
    var logger = winston_1.getLogger("main");
    logger.info("Express server running on port 3000");
});
//# sourceMappingURL=server.js.map