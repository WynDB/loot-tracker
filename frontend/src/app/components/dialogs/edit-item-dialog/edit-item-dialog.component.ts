import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IItem } from 'src/app/services/item.service';
import {ItemTypes, ItemAvailabilities}  from "../../../common/item.common";

@Component({
  selector: 'app-edit-item-dialog',
  templateUrl: './edit-item-dialog.component.html',
  styleUrls: ['./edit-item-dialog.component.scss']
})
export class EditItemDialogComponent implements OnInit {
  itemTypes = ItemTypes;
  itemAvailabilities = ItemAvailabilities;

  item: IItem;
  constructor(public dialogRef: MatDialogRef<EditItemDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: IItem) { }

  ngOnInit() {
    this.item = {...this.data};
  }
}
