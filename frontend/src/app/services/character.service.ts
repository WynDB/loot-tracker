import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment';

export interface ICharacter {
  _id?: string;
  game: string;
  name: string;
  user: any;
  equipment: string[];
}

@Injectable({
  providedIn: 'root'
})
export class CharacterService {  
  $selectedCharacter: BehaviorSubject<ICharacter> = new BehaviorSubject(undefined);
  $allCharacters: BehaviorSubject<Array<ICharacter>> = new BehaviorSubject([]);

  constructor(private http: HttpClient, private auth: AuthenticationService) { 
  }

  createCharacter(character: ICharacter){
    const token = this.auth.getToken();
    this.http.post(`${env.environment.apiUrl}/api/characters`, character, {headers: {Authorization: `Bearer ${token}`}}).subscribe((result: ICharacter)=>{
      this.$allCharacters.next(this.$allCharacters.getValue().concat([result]));
    }, (err)=>{
      console.log(err);
    });
  }

  listCharacters(gameId: string){
    const token = this.auth.getToken();
    this.http.get(`${env.environment.apiUrl}/api/characters/list/${gameId}`, {headers: {Authorization: `Bearer ${token}`}}).subscribe((result: ICharacter[])=>{
      this.$allCharacters.next(result);
    });
  }
}