import { Component, OnInit } from '@angular/core';
import { AuthenticationService, UserDetails } from 'src/app/services/authentication.service';
import { UserService, IUser } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  host: {
    class: "component-host"
  }
})
export class ProfileComponent implements OnInit {
  user: IUser;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    const userId = this.activatedRoute.snapshot.params["id"];

    this.userService.$selectedUser.subscribe((user: IUser)=>{
      this.user = user;
    });

    this.userService.selectUser(userId);
  }

  acceptInvite(gameId){
    this.userService.acceptGameInvite(this.user._id, gameId);
  }
}
