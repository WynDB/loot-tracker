import mongoose, { Document, Schema } from "mongoose";

export interface IGame {
    title: string;
    system: string;
    description: string;
    items: any[];    
    characters: any[];
    users: any[];
    invitees: any[];
}

export interface IGameModel extends IGame, Document {
    _id: string;
}

const Game: Schema = new Schema({
    title: {
        type: String
    },
    system: {
        type: String
    },
    description: {
        type: String
    },
    items: [{
        type: Schema.Types.ObjectId,
        ref: "Item"
    }],
    characters: [{
        type: Schema.Types.ObjectId,
        ref: "Character"
    }],
    users: [{
        type: Schema.Types.ObjectId,
        ref: "User"
    }],
    invitees: [{
        type: Schema.Types.ObjectId,
        ref: "User"
    }]
}, {
    timestamps: true
});

export default mongoose.model("Game", Game);