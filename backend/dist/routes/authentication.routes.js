"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var express_jwt_1 = __importDefault(require("express-jwt"));
var router = express_1.default.Router();
var controller = __importStar(require("../controllers/authentication.controller"));
var auth = express_jwt_1.default({
    secret: 'MY_SECRET',
    userProperty: "payload"
});
router.post("/register", controller.register);
router.post("/login", controller.login);
router.get("/profile/:id", auth, controller.getUser);
exports.default = router;
//# sourceMappingURL=authentication.routes.js.map