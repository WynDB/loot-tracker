import { Component, OnInit, Inject } from '@angular/core';
import { IItem } from 'src/app/services/item.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ItemTypes, ItemAvailabilities } from "../../../common/item.common";

export interface AddItemDialogData {
  item: IItem;
}

@Component({
  selector: 'app-add-item-dialog',
  templateUrl: './add-item-dialog.component.html',
  styleUrls: ['./add-item-dialog.component.scss']
})
export class AddItemDialogComponent implements OnInit {
  item: IItem;

  itemTypes = ItemTypes;
  itemAvailabilities = ItemAvailabilities;

  constructor(public dialogRef: MatDialogRef<AddItemDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: AddItemDialogData) { }

  ngOnInit() {
    this.item = this.data.item;
  }

  cancel() {
    this.dialogRef.close();
  }
}
