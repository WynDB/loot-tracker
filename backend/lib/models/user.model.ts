import mongoose, { Document, Schema } from "mongoose";
import crypto from "crypto";
import jwt from "jsonwebtoken";

export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    characters: any[];
    games: any[];
    invites: any[];
}

export interface IUserModel extends Document, IUser {
    setPassword: (password: string) => void;
    validPassword: (password: string) => boolean;
    generateJwt: () => string;
}

const User: Schema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    games: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Game"
    }],
    characters: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Character"
    }],
    invites: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Game"
    }],
    hash: {
        type: String
    },
    salt: {
        type: String
    }
});

User.methods.setPassword = function (password: string) {
    this.salt = crypto.randomBytes(16).toString("hex");
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString("hex");
}

User.methods.validPassword = function (password: string) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
}

User.methods.generateJwt = function () {
    const expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        firstName: this.firstName,
        lastName: this.lastName,
        exp: parseInt((expiry.getTime() / 1000).toString())
    }, "MY_SECRET");
}

export default mongoose.model("User", User);