import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import * as env from '../../environments/environment';

export interface IGame {
  _id: string;
  title: string;
  system: string;
  description: string;
  items: any[];    
  characters: any[];
  users: any[];
  invitees: any[];
}

@Injectable({
  providedIn: 'root'
})
export class GameService {
  $allGames: BehaviorSubject<Array<IGame>> = new BehaviorSubject([]);
  $selectedGame: BehaviorSubject<IGame> = new BehaviorSubject(undefined);

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  createGame(game: IGame, cb){
    const token = this.auth.getToken();
    this.http.post(`${env.environment.apiUrl}/api/games`, game, {headers: {Authorization: `Bearer ${token}`}}).subscribe((result: IGame)=>{
      this.$allGames.next(this.$allGames.getValue().concat([result]));
    }, (err)=>{
      console.log(err);
    });
  }

  getGame(id){
    const token = this.auth.getToken();
    this.http.get(`${env.environment.apiUrl}/api/games/${id}`, {headers: {Authorization: `Bearer ${token}`}}).subscribe((game: IGame)=>{
      this.$selectedGame.next(game);
    }, (err)=>{
      console.log(err);
    });
  }

  listGames(){
    const token = this.auth.getToken();
    this.http.get(`${env.environment.apiUrl}/api/games`, {headers: {Authorization: `Bearer ${token}`}}).subscribe((games: IGame[])=>{
      this.$allGames.next(games);
    }, (err)=>{
      console.log(err);
    });
  }

  deleteGame(id, cb){
    const token = this.auth.getToken();
    this.http.delete(`${env.environment.apiUrl}/api/games/${id}`, {headers: {Authorization: `Bearer ${token}`}}).subscribe((result)=>{
      console.log(result);
      if(cb){
        cb();
      }
    }, (err)=>{
      console.log(err);
    });
  }
}