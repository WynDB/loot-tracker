import express from "express";
const router = express.Router();

import * as controller from "../controllers/user.controller";

router.post("", controller.create);
router.get("/:id", controller.read);
router.put("/:id", controller.update);
router.put("/invite", controller.invite);
router.put("/acceptinvite/:id", controller.acceptInvite);
router.delete("/:id", controller.remove);

export default router;