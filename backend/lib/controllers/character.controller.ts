import Character from "../models/character.model";
import Game, { IGameModel } from "../models/game.model";
import User, { IUserModel } from "../models/user.model";
import { Response, NextFunction } from "express";

export const create = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (!req.payload) {
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id) as IUserModel;
        if (!user) {
            return res.status(404).send(`Invalid User.`);
        }

        //First get the game the character is supposed to be a part of. If the user does not have access to that game, the process should not complete.
        if (!req.body.game) {
            return res.status(404).send(`Invalid Game.`);
        }

        const game = await Game.findById(req.body.game) as IGameModel;
        if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
            return res.status(404).send(`You are not a part of this Game.`);
        }

        const character = await Character.create({
            name: req.body.name,
            game: game._id,
            user: req.payload._id
        });

        user.characters.push(character._id);
        await user.save();

        game.characters.push(character._id);
        await game.save();

        return res.json(character);
    } catch (err) {
        next(err);
    }
}

export const read = async (req: any, res: Response, next: NextFunction) => {
    try {
        res.send(true);
    } catch (err) {
        next(err);
    }
}

export const update = async (req: any, res: Response, next: NextFunction) => {
    try {
        res.send(true);
    } catch (err) {
        next(err);
    }
}

export const remove = async (req: any, res: Response, next: NextFunction) => {
    try {
        res.send(true);
    } catch (err) {
        next(err);
    }
}

export const list = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (!req.payload) {
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.payload._id) as IUserModel;
        if (!user) {
            return res.status(404).send(`Invalid User.`);
        }

        const game = await Game.findById(req.params.id).populate({ path: "characters", model: Character }) as IGameModel;
        if (game.users.map(c => c.toString()).indexOf(req.payload._id) < 0) {
            return res.status(404).send(`You are not a part of this Game.`);
        }

        return res.json(game.characters);
    } catch (err) {
        next(err);
    }
}