import mongoose, { Document, Schema } from "mongoose";

export interface ICharacter {
    name: string;
    class: string;
    user: any;
    game: any;
    items: any[];
}

export interface ICharacterModel extends ICharacter, Document {
    _id: string;
}

const Character: Schema = new Schema({
    name: {
        type: String
    },
    class: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    game: {
        type: Schema.Types.ObjectId,
        ref: "Game"
    },
    items: [{
        type: Schema.Types.ObjectId,
        ref: "Item"
    }]
}, {
    timestamps: true
});

export default mongoose.model("Character", Character);