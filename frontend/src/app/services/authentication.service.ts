import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import * as env from '../../environments/environment';

export interface UserDetails {
  _id: string;
  email: string;
  firstName: string;
  lastName: string;
  organisation: string;
  exp: number;
  iat: number;
}

interface TokenResponse {
  token: string;
}

export interface TokenPayload {
  email: string;
  password: string;
  firstName?: string;
  lastName?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  token: string;
  loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: HttpClient, private router: Router) { }

  private saveToken(token: string): void {
    localStorage.setItem('mean-token', token);
    this.token = token;
  }

  public getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('mean-token');
    }
    return this.token;
  }

  private request(method: 'post' | 'get', type: 'login' | 'register' | 'profile', user?: TokenPayload): Observable<any> {
    let base: Observable<any>;

    if (method === 'post') {
      base = this.http.post(`${env.environment.apiUrl}/api/profile/${type}`, user);
    } else {
      base = this.http.get(`${env.environment.apiUrl}/api/profile/${type}`, { headers: { Authorization: `Bearer ${this.getToken()}` } });
    }

    const request = base.pipe(map((data: TokenResponse) => {
      if (data.token) {
        this.saveToken(data.token);
        this.loggedIn.next(true);
      }
      return data;
    }));

    return request;
  }

  public register(user: TokenPayload): Observable<any>{
    return this.request('post', 'register', user);
  }

  public login(user: TokenPayload){
    return this.request('post', 'login', user);
  }

  public profile(): Observable<any>{
    return this.request('get', 'profile');
  }

  public logout(): void {
    this.token = '';
    localStorage.removeItem('mean-token');
    this.loggedIn.next(false);
    this.router.navigateByUrl('/');
  }

  public getUserDetails(): UserDetails {
    const token = this.getToken();

    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  public isLoggedIn() {
    const user = this.getUserDetails();
    if (user) {
      this.loggedIn.next(true);
      return user.exp > Date.now() / 1000;
    } else {
      this.loggedIn.next(false);
      return false;
    }
  }
}
