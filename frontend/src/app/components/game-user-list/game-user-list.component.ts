import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { GameService, IGame } from 'src/app/services/game.service';
import { CharacterService, ICharacter } from 'src/app/services/character.service';
import { ItemService, IItem } from 'src/app/services/item.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-game-user-list',
  templateUrl: './game-user-list.component.html',
  styleUrls: ['./game-user-list.component.scss']
})
export class GameUserListComponent implements OnInit {
  game: IGame;
  inviteEmail: string;

  constructor(
    public matDialog: MatDialog, 
    private activatedRoute: ActivatedRoute, 
    private gameService: GameService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.gameService.$selectedGame.subscribe((game: IGame) => {
      this.game = game;

      if(this.game){
      }
    });

    const id = this.activatedRoute.snapshot.params["id"];
    this.gameService.getGame(id);
  }

  inviteToGame(){
    this.userService.inviteToGame(this.inviteEmail, this.game._id);
  }
}
