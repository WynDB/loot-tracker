import express from "express";
import jwt from "express-jwt";
const router = express.Router();

import authentication from "./authentication.routes";
import game from "./game.routes";
import item from "./item.routes";
import character from "./character.routes";
import user from "./user.routes";

const auth = jwt({
    secret: 'MY_SECRET',
    userProperty: "payload"
});

router.use("/profile", authentication);
router.use("/games", auth, game);
router.use("/items", auth, item);
router.use("/characters", auth, character);
router.use("/users", auth, user);

export default router;