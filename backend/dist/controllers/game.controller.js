"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var game_model_1 = __importDefault(require("../models/game.model"));
var user_model_1 = __importDefault(require("../models/user.model"));
var character_model_1 = __importDefault(require("../models/character.model"));
var item_model_1 = __importDefault(require("../models/item.model"));
exports.create = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.create({
                        title: req.body.title,
                        system: req.body.system,
                        description: req.body.description,
                        users: [user._id]
                    })];
            case 2:
                game = _a.sent();
                if (!user.games) {
                    user.games = [];
                }
                user.games.push(game._id);
                return [4 /*yield*/, user.save()];
            case 3:
                _a.sent();
                res.status(200).json(game);
                return [3 /*break*/, 5];
            case 4:
                err_1 = _a.sent();
                console.log(err_1);
                next(err_1);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.list = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("Listing Games");
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id).populate("games").exec()];
            case 2:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                res.status(200).json(user.games);
                return [3 /*break*/, 4];
            case 3:
                err_2 = _a.sent();
                next(err_2);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.read = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id)];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.params.id)
                        .populate({ path: "characters", model: character_model_1.default, select: "name" })
                        .populate({ path: "items", model: item_model_1.default, select: "name" })
                        .populate({ path: "invitees", model: user_model_1.default, select: "email" }).exec()];
            case 2:
                game = _a.sent();
                if (!game) {
                    return [2 /*return*/, res.status(404).send("Game could not be found.")];
                }
                if (user.games.indexOf(game._id.toString()) >= 0) {
                    return [2 /*return*/, res.status(200).json(game)];
                }
                else {
                    return [2 /*return*/, res.status(404).send("Unauthorised: You do not have access to this game.")];
                }
                return [3 /*break*/, 4];
            case 3:
                err_3 = _a.sent();
                next(err_3);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.update = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, err_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id).populate("games").exec()];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.params.id)];
            case 2:
                game = _a.sent();
                if (!game) {
                    return [2 /*return*/, res.status(404).send("Game could not be found.")];
                }
                if (!(user.games.indexOf(game._id) >= 0)) return [3 /*break*/, 4];
                game.title = req.body.title;
                game.description = req.body.description;
                game.system = req.body.system;
                return [4 /*yield*/, game.save()];
            case 3:
                _a.sent();
                return [2 /*return*/, res.status(200).send(true)];
            case 4: return [2 /*return*/, res.status(404).send("Unauthorised: You do not have access to this game.")];
            case 5: return [3 /*break*/, 7];
            case 6:
                err_4 = _a.sent();
                next(err_4);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
exports.remove = function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var user, game, err_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                if (!req.payload) {
                    return [2 /*return*/, res.status(401).send("Unauthorised: Please log in.")];
                }
                return [4 /*yield*/, user_model_1.default.findById(req.payload._id).populate("games").exec()];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, res.status(404).send("Invalid User.")];
                }
                return [4 /*yield*/, game_model_1.default.findById(req.params.id)];
            case 2:
                game = _a.sent();
                if (!game) {
                    return [2 /*return*/, res.status(404).send("Game could not be found.")];
                }
                if (!(user.games.indexOf(game._id) >= 0)) return [3 /*break*/, 4];
                return [4 /*yield*/, game_model_1.default.findOneAndRemove({ _id: game._id })];
            case 3:
                _a.sent();
                return [2 /*return*/, res.status(200).send(true)];
            case 4: return [2 /*return*/, res.status(404).send("Unauthorised: You do not have access to this game.")];
            case 5: return [3 /*break*/, 7];
            case 6:
                err_5 = _a.sent();
                next(err_5);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=game.controller.js.map