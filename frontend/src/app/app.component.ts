import { Component, OnInit } from '@angular/core';
import { ICharacter, CharacterService } from './services/character.service';
import { AuthenticationService, UserDetails } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  host: {
    class: "component-host"
  }
})
export class AppComponent implements OnInit {
  title = 'Loot Tracker';
  
  loggedIn: boolean;
  profile: UserDetails;

  constructor(private auth: AuthenticationService, private router: Router){
    this.auth.isLoggedIn();
    this.auth.loggedIn.subscribe((loggedIn)=>{
      this.loggedIn = loggedIn;
      if(true){
        this.profile = this.auth.getUserDetails();
      }
    });
  }

  ngOnInit(){
  }

  goHome(){
    this.router.navigateByUrl('/');
  }

  goToLogIn(){
    this.router.navigateByUrl('/login');
  }

  goToRegister(){
    this.router.navigateByUrl('/register');
  }
  
  goToDetails(){
    this.router.navigateByUrl('/profile');
  }

  logout(){
    this.auth.logout();
  }
}
