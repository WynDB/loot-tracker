import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IItem, ItemService } from 'src/app/services/item.service';
import { ItemTypes, ItemAvailabilities } from "../../../common/item.common";
import { MatDialog } from '@angular/material';
import { EditItemDialogComponent } from '../../dialogs/edit-item-dialog/edit-item-dialog.component';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {
  @Input("item") item: IItem;
  @Output("claim") claim: EventEmitter<void> = new EventEmitter();
  @Output("release") release: EventEmitter<void> = new EventEmitter();
  @Output("save") save: EventEmitter<IItem> = new EventEmitter();

  constructor(public matDialog: MatDialog) { }

  ngOnInit() {
  }

  claimItem() {
    this.claim.emit();
  }

  releaseItem() {
    this.release.emit();
  }

  getItemTypeLabel(value) {
    const matchingType = ItemTypes.find((it) => it.value === value);
    return matchingType ? `${matchingType.label[0]}${matchingType.label.slice(1).toLowerCase()}` : "---";
  }

  getItemDescription(value: string){
    return (value || "").replace(/\n/g, "<br />");
  }

  getItemAvailabilityLabel(value) {
    const matchingType = ItemAvailabilities.find((it) => it.value === value);
    return matchingType ? matchingType.label.toLowerCase() : "---";
  }

  getItemAttunementLabel(item: IItem) {
    let attunementText = "";

    if (item.requiresAttunement) {
      attunementText = "(requires attunement";

      if(item.attunementRestrictions){
        attunementText+= ` by a ${item.attunementRestrictions}`;
      }

      attunementText += ")";
    }
    return attunementText;
  }

  editItem() {
    const dialogRef = this.matDialog.open(EditItemDialogComponent, { data: this.item });

    dialogRef.afterClosed().subscribe((item) => {
      if (item) {
        this.save.emit(item);
      }
    });
  }
}
