"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importStar(require("mongoose"));
var Item = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    type: {
        type: String,
    },
    quantity: {
        type: Number
    },
    magical: {
        type: Boolean
    },
    availability: {
        type: String
    },
    requiresAttunement: {
        type: Boolean
    },
    attunementRestrictions: {
        type: String
    },
    game: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Game"
    },
    claimedBy: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Character"
    }
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model("Item", Item);
//# sourceMappingURL=item.model.js.map