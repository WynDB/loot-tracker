import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
    class: "component-host"
  }
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
