"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var routes = express_1.default.Router();
var controller = __importStar(require("../controllers/item.controller"));
routes.post("", controller.create);
routes.get("/:id", controller.read);
routes.get("/list/:id", controller.list);
routes.put("", controller.update);
routes.put("/claim/:id", controller.claim);
routes.put("/release/:id", controller.release);
routes.delete("/:id", controller.remove);
exports.default = routes;
//# sourceMappingURL=item.routes.js.map