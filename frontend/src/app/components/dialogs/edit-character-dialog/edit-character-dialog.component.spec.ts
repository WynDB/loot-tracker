import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCharacterDialogComponent } from './edit-character-dialog.component';

describe('EditCharacterDialogComponent', () => {
  let component: EditCharacterDialogComponent;
  let fixture: ComponentFixture<EditCharacterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCharacterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCharacterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
