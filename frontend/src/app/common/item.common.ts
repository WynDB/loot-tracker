export const ItemAvailabilities = [{
    value: "common",
    label: "Common"
},{
    value: "uncommon",
    label: "Uncommon"
},{
    value: "rare",
    label: "Rare"
},{
    value: "veryrare",
    label: "Very Rare"
},{
    value: "legendary",
    label: "Legendary"
},{
    value: "artefact",
    label: "Artefact"
},{
    value: "unique",
    label: "Unique"
}]

export const ItemTypes = [{
    value: "weapon",
    label: "Weapon"
  },{
    value: "armour",
    label: "Armour"
  },{
    value: "tool",
    label: "Tool"
  },{
    value: "gear",
    label: "Adventuring Gear"
  },{
    value: "animal",
    label: "Animal"
  },{
    value: "trade",
    label: "Trade Goods"
  },{
    value: "wondrous",
    label: "Wondrous Item"
  },{
    value: "quest",
    label: "Quest Item"
  }];