import { Component, OnInit, Inject } from '@angular/core';
import { IGame } from 'src/app/services/game.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface AddGameDialogData{
  game: IGame;
}

@Component({
  selector: 'app-add-game-dialog',
  templateUrl: './add-game-dialog.component.html',
  styleUrls: ['./add-game-dialog.component.scss']
})
export class AddGameDialogComponent implements OnInit {
  game: IGame;

  constructor(public dialogRef: MatDialogRef<AddGameDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: AddGameDialogData) { 
  }

  ngOnInit() {
    this.game = {...this.data.game};
  }

  cancel(){
    this.dialogRef.close();
  }
}
