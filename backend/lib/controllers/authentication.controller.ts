import { Request, Response, NextFunction } from "express";
import User, { IUserModel } from "../models/user.model";
import passport = require("passport");

export const register = async (req: Request, res: Response, next: NextFunction) => {
    try{
        console.log(`---Registering user: ${req.body.email}`);

        const user = new User() as IUserModel;

        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;

        user.setPassword(req.body.password);

        await user.save();

        const token = user.generateJwt();
        return res.status(200).json({message: `User registered: ${req.body.email}`, token: token});
    }catch(err){
        console.log(err);
        next(err);
    }
}

export const login = async (req: Request, res: Response, next: NextFunction) => {
    try{
        passport.authenticate('local', function(user: IUserModel, info, err){
            let token;

            if(err){
                return res.status(404).json(err);
            }

            if(user){
                token = user.generateJwt();
                return res.status(200).json({token: token});
            }else{
                return res.status(404).send(`Incorrect username or password`);
            }
        })(req, res);
    }catch(err){
        next(err);
    }
}

export const getUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        next();
    }catch(err){
        next(err);
    }
}