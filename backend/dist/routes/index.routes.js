"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var express_jwt_1 = __importDefault(require("express-jwt"));
var router = express_1.default.Router();
var authentication_routes_1 = __importDefault(require("./authentication.routes"));
var game_routes_1 = __importDefault(require("./game.routes"));
var item_routes_1 = __importDefault(require("./item.routes"));
var character_routes_1 = __importDefault(require("./character.routes"));
var user_routes_1 = __importDefault(require("./user.routes"));
var auth = express_jwt_1.default({
    secret: 'MY_SECRET',
    userProperty: "payload"
});
router.use("/profile", authentication_routes_1.default);
router.use("/games", auth, game_routes_1.default);
router.use("/items", auth, item_routes_1.default);
router.use("/characters", auth, character_routes_1.default);
router.use("/users", auth, user_routes_1.default);
exports.default = router;
//# sourceMappingURL=index.routes.js.map