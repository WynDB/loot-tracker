import { Request, Response, NextFunction } from "express";

import User, { IUserModel } from "../models/user.model";
import Game, { IGameModel } from "../models/game.model";

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        next();
    } catch (err) {
        next(err);
    }
}

export const read = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (!req.payload) {
            return res.status(401).json({
                message: `Unauthorised Error: private profile`
            });
        } else {
            const user = await User.findById(req.payload._id).populate({path: "invites", model: Game}).exec();
            return res.status(200).json(user);
        }
    } catch (err) {
        next(err);
    }
}

export const update = async (req: Request, res: Response, next: NextFunction) => {
    try {
        next();
    } catch (err) {
        next(err);
    }
}

export const invite = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (!req.payload) {
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findOne({ email: req.body.email }) as IUserModel;
        if (!user) {
            return res.status(404).send(`User not found.`);
        }

        const game = await Game.findById(req.body.game) as IGameModel;
        if (!game) {
            return res.status(404).send(`Game not found.`);
        }

        if (user.invites.find(i => i == game._id)) {
            return res.status(500).send(`Invite already sent.`);
        }

        game.invitees.push(user._id);
        await game.save();

        user.invites.push(game._id);
        await user.save();

        return res.status(200).send(true);
    } catch (err) {
        return next(err);
    }
}

export const acceptInvite = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (!req.payload) {
            return res.status(401).send(`Unauthorised: Please log in.`);
        }

        const user = await User.findById(req.params.id) as IUserModel;
        console.log(user);
        if (!user) {
            return res.status(404).send(`User not found.`);
        }

        const game = await Game.findById(req.body.game) as IGameModel;
        if (!game) {
            return res.status(404).send(`Game not found.`);
        }

        console.log(game.invitees);
        if (!game.invitees.find(i => {return i == user._id.toString()})) {
            return res.status(500).send(`User not invited to game.`);
        }

        Game.updateOne({ _id: game._id }, { $pull: { invitees: user._id } });
        game.users.push(user._id);
        await game.save();

        User.updateOne({ _id: user._id }, { $pull: { invites: game._id } });
        user.games.push(game._id);
        await user.save();

        return res.status(200).send(true);
    } catch (err) {
        return next(err);
    }
}

export const remove = async (req: Request, res: Response, next: NextFunction) => {
    try {
        next();
    } catch (err) {
        next(err);
    }
}